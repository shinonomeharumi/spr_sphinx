# README #

Pocket Sphinxを使ったSPRの音声の部分のコードです.

## spr_p.py ##

Pocket Sphinxを用いて認識した,音声認識結果を送るファイルです.

## spr_main.py ##

音声認識結果を受け取って,適当な答えを決定するファイルです.

## spr_sound.py ##

spr_main.pyからメッセージを受け取って,答えの発話やビープ音の再生などの音の処理をするファイルです.
