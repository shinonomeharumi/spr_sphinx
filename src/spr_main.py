#!/usr/bin/env python
# -*- coding: utf-8 -*-
# SPR,音声,SPRのメイン

import os
import rospy
from std_msgs.msg import String, Bool, Int32
from std_msgs.msg import Float64MultiArray
import difflib # 類似度を計算するライブラリ

qa_dict = {}
PATH = './group_work.csv' # 問題と答えが書かれているcsvファイル
speak_flag = ''
localization_flag = ''
angle = ''
locaization_on = False # 音源定位ありのときTrueになる
counter = 0 # 回数をカウント
turtle = 2

# 類似度を計算する関数
def get_similar(listX, listY):
	s = difflib.SequenceMatcher(None, listX, listY).ratio()
	return s

# 受け取った質問文を処理する関数
def callback(data):
	global locaization_on
	global counter
	global angle
	question = data.data # 受け取った文をquestionとおく
	counter += 1
	print counter
	pub_stop.publish(False) # Falseを送り、音声認識ストップ
	#wait("speak_flag") # 'recognition'が返ってくるまで待つ
	beep_stop.publish('stop_recognition_beep') # 音声認識終了のビープ音を鳴らす
	wait("speak_flag")

	#質問文と問題リストの類似度計算し一番類似度の高い答えを探す
	max = 0
	answer = ''
	question = question.decode('utf-8')
	for q,a in qa_dict.items():
		level = get_similar(question,q)
		if level > max:
			max = level
			answer = a
	if counter == 3:
		pub_localization.publish('next')
	if counter == 4: # 4問目以降音源定位あり（ゆくゆくはURG?）
		locaization_on = True
	if locaization_on == True: #--- 音源定位ありの時の処理
		#pub_time.publish('recognition_stop')
		print answer
		pub_localization.publish('start')
		wait("angle")
		print angle

        # 制御に角度のメッセージ送る
		#a = [0,0,angle,1]
		a = Float64MultiArray()
		a.data.append(0)
		a.data.append(0)
		a.data.append(float(angle))
		a.data.append(1)
		move.publish(a)
		wait("turtlebot_flag") # 'localization'が返ってくるまで待つ(制御が終わったら'localization'を送ってもらう)
		pub_speak.publish(answer)
		wait("speak_flag")
		pub_start.publish(True) # 「音声認識再開」のメッセージ
		pub_time.publish('recognition_start') # 「音源定位再開」のメッセージ
	else: #--- リドルゲームの処理
		print answer
		pub_speak.publish(answer) # 発話の文字列を送る
		wait("speak_flag") # 'speaker'が返ってくるまで待つ
		pub_start.publish(True)


# 「発話終了」のメッセージを受け取る関数
def speak_signal(message):
	global speak_flag
	speak_flag = message.data
	print speak_flag

def localization(message):
	global localization_flag
	localization_flag = message.data
	print localization_flag

# 合図を受け取るまで待つための関数
def wait(target):
	if target == "speak_flag":
		global speak_flag
		speak_flag = ''
		while (speak_flag != 'speaker'):
			pass
	if target == "localization_flag":
		global localization_flag
		localization_flag = ''
		while (localization_flag != 'localization'):
			pass
	if target == "angle":
		global angle
		angle = ''
		while (angle == ''):
			pass
	if target == "turtlebot_flag":
		global turtle
		turtle = 2
		while (turtle != 0):
			pass
# 音の聞こえている角度を受け取る関数
def callangle(data):
	global angle
	angle = data.data

def move_signal(data):
	global turtle
	turtle = data.data

if __name__ == '__main__':
	with open(PATH,'r') as f: # 問題と答えが書かれたファイルを開く(パスは適宜書き換える)
		qa_list = f.readlines()
	for qa in qa_list: # 辞書qa_dict{}に問題と答えを登録
		qa = qa.rstrip().decode('utf-8').split(';')
		qa_dict[qa[0]] = qa[1]

	pub_speak = rospy.Publisher('speaker', String, queue_size=10) # 発話する文字列を送る
	move = rospy.Publisher('/move/amount', Float64MultiArray, queue_size=10)
	beep_stop = rospy.Publisher('stop', String, queue_size=10) # 音声認識終了のビープ音
	pub_start = rospy.Publisher('recognition_start', Bool, queue_size=10) # pocketsphinxの操作側に指示を送る(start)
	pub_stop = rospy.Publisher('recognition_stop', Bool, queue_size=10) # pocketsphinxの操作側に指示を送る(stop)
	pub_localization = rospy.Publisher('localization_start', String, queue_size=10) # 「ブラインドマンゲームの開始」を送る
	pub_time = rospy.Publisher('time', String, queue_size=10)
	rospy.Subscriber('finish',String,speak_signal) # 「発話終了」を受けとる
	rospy.Subscriber('angle', String, callangle) # 角度を受け取る
	#rospy.Subscriber('localization', String, localization) # turtlebotが発話した人のほうを向いたら受け取る
	rospy.Subscriber('speech', String, callback) # 音声認識の結果を受け取る
	rospy.Subscriber('/move/signal', Int32, move_signal) 
	rospy.init_node('main', anonymous=True)
	rospy.spin()
