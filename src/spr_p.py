#!/usr/bin/env python
# -*- coding: utf-8 -*-
# SPR,音声,音声認識

import rospy
from std_msgs.msg import String, Bool
import os
from pocketsphinx import LiveSpeech

speech_recognition = True

def riddlegame_start(data):
    talker()

def talker():
    model_path = '/usr/local/lib/python2.7/dist-packages/pocketsphinx/model'
    while 1:
        global speech_recognition
        #--- 音声認識
        if speech_recognition == True:
            print('true')
            speech = LiveSpeech(
            verbose=False,
            sampling_rate=8000,
            buffer_size=2048,
            no_search=False,
            full_utt=False,
            hmm=os.path.join(model_path, 'en-us'),
            lm=os.path.join(model_path, 'en-us.lm.bin'),
            dic=os.path.join(model_path, 'group_work.dic') # 使用する辞書を指定
            )
        #--- 音声認識をストップする
        if speech_recognition == False:
            #print('false')
            speech = LiveSpeech(
            verbose=False,
            sampling_rate=8000,
            buffer_size=2048,
            no_search=True,
            full_utt=False,
            hmm=os.path.join(model_path, 'en-us'),
            lm=os.path.join(model_path, 'en-us.lm.bin'),
            dic=os.path.join(model_path, 'group_work.dic')
            )

        if speech_recognition == True:
            for text in speech:
                text = str(text) # 認識結果
                print(text)
                pub.publish(text) # 音声認識の結果をメインに送る
                break

        if speech_recognition == False: # Trueになるまで音声認識をストップしたまま待機する
            while speech_recognition != True:
                pass
        continue

# 「音声認識開始」のメッセージを受け取る関数
def control(data):
    global speech_recognition
    speech_recognition = data.data
    print(speech_recognition)

# 「音声認識停止」のメッセージを受け取る関数
def control_stop(data):
    global speech_recognition
    speech_recognition = data.data
    print(speech_recognition)

if __name__ == '__main__':
    pub = rospy.Publisher('speech', String, queue_size=10) # 音声認識の結果をメインに送る
    rospy.Subscriber('spr_start', Bool, riddlegame_start) # 「リドルゲーム開始」を受け取る
    rospy.Subscriber('recognition_start', Bool, control) # 「音声認識開始」を受け取る
    rospy.Subscriber('recognition_stop', Bool, control_stop) # 「音声認識ストップ」を受ける取る
    rospy.init_node('talker', anonymous=True)
    
    talker()

    rospy.spinonce()
