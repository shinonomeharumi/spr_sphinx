#!/usr/bin/env python
# -*- coding: utf-8 -*-
# SPR,音声,答えの発話

import os
import rospy
from std_msgs.msg import String
import subprocess

# 発話する文字列を受け取る関数
def callback(data):
    answer = data.data # 答えの文
    if answer is not '':
        os.system('espeak "{}" -s 135'.format(answer)) # 発話
        pub_finish.publish('speaker')
        subprocess.call("aplay ./start.wav", shell=True) # ビープ音(wavファイル)再生
    else:
        os.system("espeak 'I can not find the answer' -s 135")
        pub_finish.publish('speaker')
        subprocess.call("aplay ./start.wav", shell=True) # ビープ音(wavファイル)再生

# 音声をストップした時にビープ音を鳴らす関数
def stop_beep(data):
    text = data.data
    if text == 'stop_recognition_beep':
        subprocess.call("aplay ./stop.wav", shell=True) # ビープ音(wavファイル)再生
        pub_finish.publish('speaker')

if __name__ == '__main__':
    pub_finish = rospy.Publisher('finish', String, queue_size=10) # 「発話終了」を知らせる
    rospy.Subscriber('speaker', String,callback) # 発話する文を受け取る
    rospy.Subscriber('stop', String,stop_beep) # 「音声認識終了」を受け取る
    rospy.init_node('listener', anonymous=True)
    rospy.spin()
